﻿using System;
namespace mySchool
{
	class ParentClass
	{

		public string ParentIdCode;
		public string Pname;
		public string ChildId;

		public ParentClass(string idCode, string name, string childId)
		{
			ParentIdCode = idCode;
			Pname = name;
			ChildId = childId;

		}
		public string ParentName()
		{
			return Pname;
		}
		public string ParentID()
		{
			return ParentIdCode;
		}
		public string ChildID()
		{
			return ChildId;
		}
		public override string ToString()
		{
			return ($"{Pname} {ParentIdCode} {ChildId}");
		}

	}
}
