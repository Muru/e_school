﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mySchool
{
	class StudentClass
	{

		public string StudentIdCode;
		public string Sname;
		public string Level;
		public List<GradeClass> Grades = new List<GradeClass>();

		public StudentClass(string idCode, string name, string level)
		{
			StudentIdCode = idCode;
			Sname = name;
			Level = level;

		}

		public string StudentName()
		{
			return Sname;
		}

		public string StudentID()
		{
			return StudentIdCode;
		}

		public string StudentLevel()
		{
			return Level;
		}

		public void AddCourse(GradeClass Grade)
		{
			Grades.Add(Grade);
		}

		public void ShowCourses()
		{
			foreach (var item in Grades)
			{
                var gradeAverage = item.Average();
                Console.WriteLine($"COURSE: {item.Course}, TEACHER: {item.Teacher}, GRADES: {item.Sgrades}, AVERAGE: {gradeAverage} ");
			}
		}

		public override string ToString()
		{
			
			return ($"{Sname} {Level} {StudentIdCode}");
		}

	}
}
