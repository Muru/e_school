﻿using System;
using System.Collections.Generic;

namespace mySchool
{
	class GradeClass
	{

		public string IdCode;
		public string Course;
		public string Sgrades;
  
		public TeacherClass Teacher { get; set; }


		public GradeClass(string idCode, string course, string grades)
		{
			IdCode = idCode;
			Course = course;
			Sgrades = grades;

		}

  
		public string StudentId()
		{
			return IdCode;
		}
		public string StudentCourse()
		{
			return Course;
		}
		public string StudentGrade()
		{
           
            return Sgrades;
            
		}

        

        public string Average()
        {
			string sth = "3 4 5";
			int number = 0;
			string[] gradesList = sth.Split(' ');
			for (int i = 0; i < gradesList.Length; i++)
			{
				number += int.Parse(gradesList[i]);
			}
			int num = number / gradesList.Length;
			var average = num.ToString();

            return average;

		}

		public void AddTeacher(TeacherClass teacher)
		{
			Teacher = teacher;
		}
		public override string ToString()
		{
			return ($"{IdCode} {Course} {Sgrades}");
		}

	}
}
