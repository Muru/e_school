﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mySchool
{
	class Program
	{
		// For Mac
		//static string FileStudent = (@"../../students.txt");
		//static string FileParent = (@"../../parents.txt");
		//static string FileTeacher = (@"../../teachers.txt");
	 //   static string FileGrade = (@"../../grades.txt");

        //For Windows
		static string FileStudent = (@"..\..\students.txt");
		static string FileParent = (@"..\..\parents.txt");
		static string FileTeacher = (@"..\..\teachers.txt");
		static string FileGrade = (@"..\..\grades.txt");

		public static Dictionary<string, StudentClass> students = ReadStudents();
		public static Dictionary<string, ParentClass> parents = ReadParents();
		public static Dictionary<string, TeacherClass> teachers = ReadTeachers();
		public static List<GradeClass> grades = ReadGrades();

		public static string userId;

		public static void Main()
		{
			AddGradesToStudents(students, grades);
			AddTeachersToGrades(teachers, grades);

            Beginning();			
		}



        public static void Beginning()
        {
            Console.WriteLine("You are logged out.");
            Console.WriteLine("Enter as a teacher(t)47902156985, parent(p)35506124636 or a student(s)48606294447 or quit(q): ");
			var user = Console.ReadLine().ToLower();
            if (string.Equals(user, "q"))
			{
                Environment.Exit(0);
			}
			Console.WriteLine("Please enter your ID");
			userId = Console.ReadLine();

			Console.Clear();

		    switch (user)
			{
				case "t":
					TeacherEntry();
					break;
				case "p":
					ParentEntry();
					break;
				case "s":
					StudentEntry();
					break;
				default:
					Console.WriteLine("Please enter the correct letter: ");
					break;
			}
        }


        //generating the dates for the grades
        public static Random randomDate = new Random();
        public static void RandomDay()
        {
            DateTime stop = new DateTime(2007, 7, 21);
            DateTime start = new DateTime(2006, 9, 1);
            int range = (stop - start).Days;
            DateTime longDate = start.AddDays(randomDate.Next(range));
            var shortDate = longDate.ToShortDateString();
            Console.WriteLine(shortDate);
        }



        public static void TeacherEntry()
		{
            if (!teachers.ContainsKey(userId))
            {
                Console.WriteLine("Please enter correct ID");
            }
            else
            {
                var aTeacher = teachers[userId];

                Console.WriteLine(aTeacher.GetType().Name);
                Console.WriteLine($"Welcome {aTeacher.Tname} \n");
                Console.WriteLine($"You are teaching {aTeacher.Tcourse} \n ");
                Console.WriteLine($"Your students are: ");
                foreach (var g in grades)
                {
                    if (string.Equals(g.Course, aTeacher.Tcourse))
                    {
                        foreach (var s in students)
                        {
                            if (string.Equals(s.Value.StudentIdCode, g.IdCode))
                            {
                                Console.WriteLine($"{s.Value.Level}, {s.Value.Sname}, GRADES: {g.Sgrades} ");
                                RandomDay();
                            }
                        }
                    }
                }
                Console.WriteLine("\n \n \n");
                Beginning();
            }
		}



		public static void ParentEntry()
		{
			var parent = parents[userId];
			Console.WriteLine($"Welcome {parent.Pname} \n");
            var childrenIds = parent.ChildId;
            var childIdList = childrenIds.Split(' ');
            var childCount = childIdList.Length;
            Console.WriteLine($"Amount of childer you have in this school: {childCount} \n");

            foreach (var child in childIdList)
            {
                var student = students[child];
                Console.WriteLine($"Your child {student.Sname} is studying in {student.Level} \n");
                Console.WriteLine("In the same class there are studying: ");

                foreach (var s in students)
                {
                    if (string.Equals(s.Value.Level, student.Level))
                    {
                        foreach (var p in parents)
                        {
                            if (string.Equals(p.Value.ChildId, s.Value.StudentIdCode))
                                Console.WriteLine($"STUDENT: {s.Value.Sname},  PARENT: {p.Value.Pname}");
                        }
                    }
                }
                Console.WriteLine("\n");
                student.ShowCourses();
                Console.WriteLine("\n \n");
            }
            Beginning();
        }


		public static void StudentEntry()
		{          
			var aStudent = students[userId];
			Console.WriteLine($"Welcome {aStudent.Sname} \n");
			Console.WriteLine($"You are studying in {aStudent.Level} \n");
			Console.WriteLine("Your classmates are: ");
            foreach (var s in students)
            {
                if (string.Equals(s.Value.Level, aStudent.Level))
                {
                    foreach (var p in parents)
                    {
                        if (string.Equals(p.Value.ChildId, s.Value.StudentIdCode))
                            Console.WriteLine($"STUDENT: {s.Value.Sname}, PARENT: {p.Value.Pname}");
                    }
                }
            }
            Console.WriteLine("\n");
            aStudent.ShowCourses();
            Console.WriteLine("\n");

            foreach (var elder in parents)
            {
                string[] children = elder.Value.ChildId.Split(' ');
                var childList = children.ToList();
                foreach (var child in childList)
                {
                    if (string.Equals(aStudent.StudentIdCode, child) && childList.Count > 1)
                    {
                        var siblings = childList.Where(x => x != child);
                        foreach (var s in siblings)
                        {                           
                            if (string.Equals(students[s].StudentIdCode, s))
                                Console.WriteLine($"Your sibling {students[s].Sname} is studying in {students[s].Level} \n");
                            Console.WriteLine($"His/Her classmates are: ");
                            foreach (var classmate in students)
                            {                               
                                if (string.Equals(classmate.Value.Level, students[s].Level))  
                                Console.WriteLine($"{classmate.Value.Sname}");
                            }
                        }
                    }
                }
            }
            Console.WriteLine("\n \n \n");
            Beginning();
		}



		static public Dictionary<string, StudentClass> ReadStudents()
		{
			var Students = File.ReadAllLines(FileStudent).Select(x => x.Split(','))
							   .ToDictionary(x => x[0], x => new StudentClass(name: x[1], idCode: x[0], level: x[2]));
			return Students;
		}

		static public Dictionary<string, ParentClass> ReadParents()
		{
			var Parents = File.ReadAllLines(FileParent).Select(x => x.Split(','))
							   .ToDictionary(x => x[0], x => new ParentClass(name: x[1], idCode: x[0], childId: x[2]));
			return Parents;
		}


		static public Dictionary<string, TeacherClass> ReadTeachers()
		{
			var Teachers = File.ReadAllLines(FileTeacher).Select(x => x.Split(','))
							   .ToDictionary(x => x[0], x => new TeacherClass(name: x[1], idCode: x[0], course: x[2]));
			return Teachers;
		}


		static public List<GradeClass> ReadGrades()
		{
			var Grades = File.ReadAllLines(FileGrade).Select(x => x.Split(','))
							 .Select(x => new GradeClass(x[0], x[1], x[2]))
							 .ToList();
			return Grades;
		}

		static public void AddGradesToStudents(Dictionary<string, StudentClass> students, List<GradeClass> grades)
		{
			foreach (var s in students)
			{
				foreach (var g in grades)
				{
					if (string.Equals(s.Value.StudentIdCode, g.IdCode))
					{
						s.Value.AddCourse(g);
					}
				}
			}
		}

		static public void AddTeachersToGrades(Dictionary<string, TeacherClass> teachers, List<GradeClass> grades)
		{
			foreach (var t in teachers)
			{
				foreach (var g in grades)
				{
					if (string.Equals(t.Value.Tcourse, g.Course))
					{
						g.AddTeacher(t.Value);
					}
				}
			}
		}

	}
}
