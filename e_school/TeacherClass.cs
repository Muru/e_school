﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace mySchool
{
	class TeacherClass
	{

		public string TeacherIdCode;
		public string Tname;
		public string Tcourse;
        //public List<GradeClass> Grades = new List<GradeClass>();
        //public Dictionary<string, StudentClass> MyStudents = new Dictionary<string, StudentClass>();

        public TeacherClass(string idCode, string name, string course)
		{
			TeacherIdCode = idCode;
			Tname = name;
			Tcourse = course;

		}

        //////testing
        ////public void AddCourse(GradeClass Grade)
        ////{
        ////    Grades.Add(Grade);
        ////}
        ////public void ShowGrades()
        ////{
        ////    foreach (var item in Grades)
        ////    {
        ////        Console.WriteLine($"course: {item.IdCode} grades: {item.Sgrades}");
        ////    }
        ////}

        ////testing
        //public void AddStudent(string IdCode, StudentClass Level)
        //{
        //    MyStudents.Add(IdCode, Level);
        //}

        ////testing
        //public void ShowStudent()
        //{
        //    foreach (var item in MyStudents)
        //    {
        //        Console.WriteLine($"Your student: {item.Value.Sname} {item.Value.Level}");
        //    }
        //}

        public string TeacherName()
		{
			return Tname;
		}
		public string TeacherID()
		{
			return TeacherIdCode;
		}
		public string TeacherCourse()
		{
			return Tcourse;
		}
		public override string ToString()
		{
			return ($"{this.TeacherIdCode}-{Tname}");
		}

	}
}
